# Tauri + Vue 3 + TypeScript

In order to run the project with `npm run tauri dev` you'll need to have an environment variable set up for 
`RUST_PASS` so that the encryption module can function. This has to be a 32 byte key, and can be generated on 
Linux by using `< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;`

## Notes
- This is very much a toy project. I want to cram in as much Rust as I can to see how nicely it all plays together
- There appears to be some weird intermittent issues with encrypting/decrypting or the UI copy string function that causes the app to die
- It is a mess in here and I _may_ clean it up. Some day.
- I would like to improve the passphrase generator and add some more config options
- I also want to figure out how to use custom menus better, so I can remove the hideous top nav
- Yes I went with stupid "H4CK3R" theme, fight me, I think it looks fun.

## What does this even do?

Well, it's primarily an encryption tool. You can encrypt text and files but there is also a steganography 
tool which currently (as of 06/05/23) supports PNGs and JPGs with planned support for a huge number of 
files.

## BEHOLD!

### The home screen
![Homepage](./public/img.png)

### Text encryption screen
Encrypt any text with XChaCha20-Poly1305
![Encrypt page](./public/img_1.png)

### Text decryption screen
Decrypt any text encrypted with this app
![Decrypt page](./public/img_2.png)

### Passphrase generation screen
Generate a passphrase with varying number of words.
![Generate passphrase page](./public/img_3.png)

### Encrypt file screen
This screen allows you to encrypt any file. It uses the same encryption as the text (XChaCha20-Poly1305) and generates
the encrypted file wherever the original file was.
![Encrypt file page](./public/img_4.png)

### Decrypt file screen
This screen decrypts any file that was encrypted with this app. It will decrypt then remove the encrypted file
once finished.
![Decrypt file page](./public/img_5.png)

### There's also a light theme
![Light theme](./public/img_white.png)