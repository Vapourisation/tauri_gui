// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

#[macro_use]
extern crate log;
extern crate core;

use rand::seq::SliceRandom;
use rand::Rng;
use rand::thread_rng;
use std::env;

mod encryption;
mod api;
mod stego;

#[tauri::command]
fn stego_encode(filepath: String, text: String, password: String) -> String {
    let encrypted = encryption::crypt::encrypt(password, text.clone()).unwrap();
    info!( "Encrypted: {:?}", encrypted.clone() );
    stego::encode( filepath, encrypted )
}

#[tauri::command]
fn stego_decode(filepath: String, password: String) -> String {
    stego::decode(filepath, password)
}

#[tauri::command]
fn encrypt_text(text: String)  -> String {
    let pass = env::var("RUST_PASS").unwrap_or(String::from("Super_Secret_Key_To_Protect_You__"));
    let encrypted = encryption::crypt::encrypt(pass, text);

    match encrypted {
        Ok(encrypted) => hex::encode(encrypted),
        Err(e) => {
            info!("{:?}", e);
            String::from("FAILED")
        },
    }
}

#[tauri::command]
fn decrypt_text(text: String)  -> Vec<u8> {
    let pass = env::var( "RUST_PASS" ).unwrap_or(String::from("Super_Secret_Key_To_Protect_You__"));
    let decrypted = encryption::crypt::decrypt(pass, text);

    match decrypted {
        Ok(decrypted) => decrypted,
        Err(e) => {
            info!("{:?}", e);
            Vec::new()
        },
    }
}

#[tauri::command]
fn encrypt_file( source: String, key: Option<String> ) -> String {
    let pass: String;
    if key.is_none() {
        pass = env::var( "RUST_PASS" ).unwrap_or( String::from( "Super_Secret_Key_To_Protect_You__" ));
    } else {
        pass = key.unwrap();
    }
    let dest_file_path = source.clone() + ".enc";
    encryption::crypt::encrypt_file(&source, &dest_file_path, pass ).unwrap();
    dest_file_path
}

#[tauri::command]
fn decrypt_file( source: String, key: Option<String> ) -> String {
    let pass: String;
    if key.is_none() {
        pass = env::var( "RUST_PASS" ).unwrap_or( String::from( "Super_Secret_Key_To_Protect_You__" ));
    } else {
        pass = key.unwrap();
    }
    let dest_file_path = &source.clone()[..source.len()-4];
    encryption::crypt::decrypt_file(&source, &dest_file_path.to_string(), pass ).unwrap();
    dest_file_path.to_string()
}

#[tauri::command]
fn generate_passphrase(words: u8, numbers: bool) -> String {
    let str = include_str!("./words.txt").replace("-", "").replace("'", "").to_ascii_lowercase();
    let combined = str.split("\n").collect::<Vec<&str>>();
    let mut password: String = String::new();

    for i in 0..words {
        let word = combined.choose(&mut rand::thread_rng()).expect("Failed to find a string");
        password.push_str(&word);

        if i < words - 1 {
            password.push_str( "-" );
        }
    }

    if numbers {
        password.push_str(&thread_rng().gen_range(10000..9999999).to_string())
    }

    return password;
}

#[tauri::command]
fn generate_password(length: u8, upper: bool, symbols: bool, numbers: bool) -> String {
    let mut rng = thread_rng();
    let upper_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let number_string = "0123456789";
    let symbol_string = "!@#$%^&*()_+-=[]{}|;:',./<>?";

    let mut combined = String::from("abcdefghijklmnopqrstuvwxyz");

    if upper {
        combined.push_str(&upper_letters)
    }

    if symbols {
        combined.push_str(&symbol_string);
    }

    if numbers {
        combined.push_str(&number_string);
    }

    let combined_chars = combined.chars().collect::<Vec<char>>();

    let mut password = String::new();
    for _ in 0..length {
        password.push(*combined_chars.choose(&mut rng).unwrap());
    }

    return password;
}

fn main() {
    tauri::Builder::default()
        .plugin(tauri_plugin_log::Builder::default().build())
        .invoke_handler(
            tauri::generate_handler![
                generate_passphrase,
                generate_password,
                encrypt_text,
                decrypt_text,
                encrypt_file,
                decrypt_file,
                stego_encode,
                stego_decode
            ]
        )
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
