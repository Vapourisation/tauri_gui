use image::{EncodableLayout, GenericImageView};
use serde::{Serialize, Deserialize};
use std::fs::File;
use std::io::prelude::*;
use bitvec::prelude::*;
use little_exif::endian::Endian::Big;
use little_exif::metadata::Metadata;
use little_exif::exif_tag::ExifTag;

// Constants
const HUFFMAN: [u8;2] = [0xff, 0xc4];
const QUANTIZATION: [u8;2] = [0xff, 0xdb];
const START: &str = "######";
const END: &str = "======";

// Structs
#[derive(Debug, Serialize, Deserialize)]
pub struct JPEG {
    pub path: String,
    pub width: u32,
    pub height: u32,
    pub alpha: bool,
    pub channels: u8
}

impl JPEG {
    pub fn new(filepath: String) -> JPEG {
        let img = image::open(&filepath).expect("File not found!");
        let (w, h) = img.dimensions();

        return JPEG {
            path: filepath,
            width: w,
            height: h,
            alpha: img.color().has_alpha(),
            channels: img.color().channel_count()
        };
    }

    #[allow(dead_code)]
    pub fn dimensions(&self) -> String {
        format!("{}W x {}H", self.width, self.height)
    }
    
    #[allow(dead_code)]
    pub fn total_pixels(&self) -> u32 {
        self.width * self.height
    }
    
    #[allow(dead_code)]
    pub fn create_thumbnail(&self, w: u32, h: u32) -> String {
        let img = image::open(self.path.clone()).expect("File not found!");
        let thumb = img.thumbnail(w, h);
    
        let path_split: Vec<&str> = self.path.split(".").collect();
        let thumb_path = format!("{}_thumb.{}", path_split[0], path_split[1]);
        let error_str = format!("Failed to save thumbnail at {:?}", thumb_path);
    
        thumb.save(&thumb_path).expect(&error_str[..]);
        return thumb_path
    }

    fn is_valid( &self, vec: [u8;4] ) -> bool {
        let mut is_valid = false;

        if vec[0] == 0xff && vec[1] == 0xd8 {
            // Checking for correct JPEG/JFIF headers
            if vec[2] == 0xff && (vec[3] == 0xe1 || vec[3] == 0xe0) {
                info!( "Found valid JPEG/JFIF file" );
                is_valid = true;
            }
        }

        return is_valid;
    }

    pub fn encode(&self,  data: Vec<u8>) -> String {
        let mut initial_huffman_found = false;
        let mut huffman_found = false;
        let mut i = 0;
        let mut j = 0;
        let mut data_size = 0;

        let mut vec = match std::fs::read(&self.path ) {
            Err(why) => return format!("couldn't open {}: {}", self.path, why),
            Ok(file) => file,
        };

        let mut bit_string: Vec<String> = Vec::new();
        let path_split: Vec<&str> = self.path.split(".").collect();
        let enc_path;

        if path_split.len() == 3 {
            enc_path = format!(".{}{}_enc.{}", path_split[0], path_split[1], path_split[2]);
        } else {
            enc_path = format!(".{}_enc.{}", path_split[0], path_split[1]);
        }

        for x in 0..data.len() {
            bit_string.push( format!("{:08b}", data[x]) );
        }

        let joined = bit_string.join("");
        let joined_as_bytes = joined.as_bytes();

        info!( "Beginning process of data" );

        // File start is correct
        if self.is_valid( [vec[0], vec[1],vec[2], vec[3]] ) {
            info!( "Found EXIF data" );
            let huffman_itr = crate::stego::utils::find_sub_vec(vec.clone(), HUFFMAN.clone().to_vec()).expect( "Failed to find quantization table data. Exiting." );

            error!( "Found huffman table at: {}", huffman_itr );

            if huffman_itr != vec.len() {
                error!( "Found Huffman table" );
                initial_huffman_found = true;
                i = huffman_itr;
            }

            if initial_huffman_found {
                error!( "Found First Huffman table. Looping to look for others..." );
                while i < vec.len() - 1
                {
                    if vec[i + 1] == HUFFMAN[0] {
                        if vec[i + 2] == HUFFMAN[1] {
                            huffman_found = true;
                            i += 2;
                        } else {
                            huffman_found = false;
                        }
                    } else {
                        if huffman_found {
                            if j < joined_as_bytes.len() {
                                if vec[i] > 110 {
                                    error!( "Before: {:?}", vec[i] );
                                    if joined_as_bytes[j] == 49 {
                                        if vec[i] % 2 == 0 {
                                            if vec[i] > 0 {
                                                vec[i] = vec[i] - 1;
                                            } else {
                                                vec[i] = vec[i] + 1;
                                            }
                                        }
                                    } else {
                                        if vec[i] % 2 != 0 {
                                            vec[i] = vec[i] - 1;
                                        }
                                    }
                                    error!( "After: {:?}", vec[i] );
                                    error!( "------------------------------------" );
                                    j += 1;
                                    data_size += 1;
                                }
                            } else {
                                i = vec.len();
                            }

                        }
                    }
                    i += 1;
                }
            }
        } else {
            error!( "No EXIF data found." );
        }

        if data_size < joined_as_bytes.len() {
            error!( "Failed to store all necessary bits. Using metadata" );
            let path = std::path::Path::new(&self.path);
            error!( "Creating metadata object from file, {}", path.display() );
            let mut metadata = Metadata::new_from_path(&path).unwrap();

            error!( "Got metadata object" );

            metadata.set_tag(
                ExifTag::ImageDescription(hex::encode(data).to_string())
            );

            error!( "Set ImageDescription tag" );

            metadata.write_to_file(&path).unwrap();
            String::from(enc_path)
        } else {
            error!( "Processed vec length: {}", vec.len() );
            let mut output = match File::create(&enc_path.clone()) {
                Err(why) => return format!("couldn't open {}: {}", self.path, why),
                Ok(file) => file,
            };
            output.write( vec.as_bytes() ).expect( "Failed to write to file" );
            String::from(enc_path)
        }
    }

    pub fn decode(&self) -> Result<Vec<u8>, crate::stego::utils::DecodeError> {
        let mut decoded: Vec<u8> = Vec::new();
        let mut decoded_string = String::new();
        let mut initial_huffman_found = false;
        let mut huffman_found = false;
        let mut i = 0;
        let mut bit_count = 0;
        let mut runs = 0;

        let mut file = File::open(&self.path)?;

        let mut vec = Vec::new();
        file.read_to_end(&mut vec)?;

        let mut binary_text = bits![mut 0; 8];

        if self.is_valid( [vec[0], vec[1],vec[2], vec[3]] ) {
            info!( "Found EXIF data" );
            let huffman_itr = crate::stego::utils::find_sub_vec(vec.clone(), HUFFMAN.clone().to_vec()).expect( "Failed to find quantization table data. Exiting." );

            if huffman_itr != vec.len() {
                info!( "Found Huffman table" );
                initial_huffman_found = true;
                i = huffman_itr + HUFFMAN.len();
            }

            if initial_huffman_found {
                info!( "Found First Huffman table. Looping to look for others..." );
                while i < vec.len()
                {
                    if vec[i] == HUFFMAN[0] {
                        if vec[i+1] == HUFFMAN[1] {
                            huffman_found = true;
                            i += 2
                        } else {
                            huffman_found = false;
                        }
                    } else {
                        if huffman_found {
                            if bit_count != 0 && bit_count % 8 == 0 {
                                // For subsequent runs, if unsuccessful 
                                if runs > 0 {
                                    binary_text.reverse();
                                } else {
                                    error!( "binary_text: {:?}", binary_text.bytes() );
                                }

                                let bytes = [binary_text.load::<u8>()];
                                match std::str::from_utf8( &bytes ) {
                                    Ok( str ) => {
                                        decoded_string.push( str.chars().next().expect( "String is empty" ) );
                                        decoded.append( &mut bytes.to_vec() );
                                        binary_text.fill( false );
                                        bit_count = 0;
                                    },
                                    Err( err ) => {
                                        binary_text.reverse();
                                        bit_count = 7;
                                    }
                                };

                                runs += 1;
                            }

                            if vec[i] % 2 == 0 {
                                binary_text.set( bit_count, false );
                            } else {
                                binary_text.set( bit_count, true );
                            }
                            
            
                            if decoded.ends_with( END.as_bytes() ) {
                                error!( "SUCCESS: Found a string" );
                                i = vec.len();
                            }
            
                            bit_count += 1;

                            i +=1 ;
                        }
                    }
                    i += 1;
                }
            }
        } else {
            info!( "No EXIF data found." );
        }

        if decoded.len() == 0 {
            info!("No data found. Trying to extract from metadata");
            let path = std::path::Path::new(&self.path);
            let metadata = Metadata::new_from_path(&path).unwrap();

            let tag = metadata.get_tag(&ExifTag::ImageDescription(String::new())).unwrap();
            let data = hex::decode(tag.value_as_u8_vec(&Big)).unwrap();
            Ok(data)
        } else {
            Ok(decoded)
        }
    }
}

pub fn create(filepath: String) -> JPEG {
    return JPEG::new(filepath);
}
