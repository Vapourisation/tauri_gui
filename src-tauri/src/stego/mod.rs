mod phash;
mod png;
mod jpeg;
mod edge_detection;
mod utils;

use crate::encryption;
use chrono;
use image::{Luma, GrayImage, DynamicImage};
use std::str;
use std::string::{String};
use hex;


const START: &str = "######";
const END: &str = "======";

pub fn edge(path: String) -> String {
    let mut height: u32 = 0;
    let mut width: u32 = 0;
    let image: DynamicImage;
    let mut out_path  = path.split("/").collect::<Vec<&str>>();
    let file_name = out_path.pop().unwrap();
    let output_name = format!("{}/{}_{}", out_path.join("/"), chrono::offset::Utc::now(), file_name);

    let ext = path.split( "." ).last().unwrap();
    if ext.contains( "png" ) {
        let img = png::create(path);
        height = img.height;
        width = img.width;
        image = img.image;
    } else if ext.contains( "jpg" ) {
        let img = png::create(path);
        height = img.height;
        width = img.width;
        image = img.image;
    } else {
        panic!("Unaccepted image type. Aborting.");
    }

    // All operations use a 2D Vec of F32 and image `.to_vec()` outputs a 1D Vec so instead we
    // create an intermediary image instance in RGB F32 and add those values to `new_img`.
    let mut new_img: Vec<Vec<f32>> = vec![vec![0.0;height as usize];width as usize];
    let temp_image = image.into_rgb32f();

    let stop_x = width as usize;
    let stop_y = height as usize;

    for x in 0..stop_x {
        for y in 0..stop_y {
            let gray = (0.2126 * temp_image.get_pixel(x as u32, y as u32)[0]) + (0.7152 * temp_image.get_pixel(x as u32, y as u32)[1]) + (0.0722 * temp_image.get_pixel(x as u32, y as u32)[2]);
            new_img[x][y] = gray;
        }
    }

    let edge = edge_detection::detect(new_img);
    let mut altered_image = GrayImage::new(width, height);

    for x in 0..stop_x {
        for y in 0..stop_y {
            let pix_arr = edge[x][y].clone();
            let data = [(pix_arr * 255.0) as u8];
            altered_image.put_pixel(x as u32, y as u32, Luma(data));
        }
    }

    debug!("Saving file to {output_name}");

    match altered_image.save(&output_name) {
        Ok(..) => debug!("Saved file"),
        Err(e) => error!("{e}")
    };

    return output_name
}

pub fn encode( path: String, data: Vec<u8> ) -> String {
    let mut encoded: String = String::new();

    let mut combined: Vec<u8> = Vec::new();
    combined.append(&mut START.as_bytes().to_vec());
    combined.append(&mut data.clone());
    combined.append(&mut END.as_bytes().to_vec());

    let ext = path.split( "." ).last().unwrap();
    if ext.contains( "png" ) {
        info!( "File is a png. Processing..." );
        let image = png::create( path );
        encoded = image.encode( combined );
    } else if ext.contains( "jpg" ) {
        info!( "File is a jpg. Processing..." );
        let image = jpeg::create( path );
        encoded = image.encode( combined );
    } else {
        panic!("Unaccepted image type. Aborting.");
    }

    return encoded;
}

pub fn decode( path: String, secret_key: String ) -> String {
    let mut decoded: Vec::<u8> = Vec::new();

    info!("Decoding {}", path);

    if path.contains( "png" ) {
        info!( "File is a png. Processing..." );
        let image = png::create( path );
        decoded = image.decode();
    } else if path.contains( "jpg" ) {
        info!( "File is a jpg. Processing..." );
        let image = jpeg::create( path );
        decoded = image.decode().unwrap();
    } else {
        panic!("Unaccepted image type. Aborting.");
    }

    info!("decoded {:?}", decoded.clone());

    return if decoded.len() > 0 {
        let plain_text = encryption::crypt::decrypt_vec(secret_key, decoded).unwrap();
        String::from_utf8(plain_text).unwrap()
    } else {
        String::from("FAILED")
    }
}
