extern crate rand;

use std::{error::Error};
use std::{num::ParseIntError};
use std::fmt::{ Display, Debug, Formatter, Result as FmtResult };
use std::io;
use std::str::Utf8Error;

pub fn decode_binary(s: &str) -> Result<Vec<u8>, ParseIntError> {
    (0..s.len())
        .step_by(9)
        .map(|i| u8::from_str_radix(&s[i..i + 8], 2))
        .collect()
}

// ---------------------------------------------------------------------------------------------------------
// Errors
// ---------------------------------------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct SearchError;

impl Error for SearchError {}

pub type SearchResult<T> = std::result::Result<T, SearchError>;

impl Display for SearchError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "Failed to find sub-vector.")
    }
}

pub enum DecodeError {
    InvalidData, 
    InvalidFile
}

impl DecodeError {
    fn message( &self ) -> &str {
        match self {
            Self::InvalidData => "Invalid Data",
            Self::InvalidFile => "Invalid File",
        }
    }
}

impl From<io::Error> for DecodeError {
    fn from( _: io::Error ) -> Self {
        Self::InvalidFile
    }
}

impl From<Utf8Error> for DecodeError {
    fn from(value: Utf8Error) -> Self {
        Self::InvalidData
    }
}

impl Display for DecodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!( f, "{}", self.message() )
    }
}

impl Debug for DecodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!( f, "{}", self.message() )
    }
}

impl Error for DecodeError {}

// ---------------------------------------------------------------------------------------------------------


pub fn find_sub_vec(vec: Vec<u8>, sub_vec: Vec<u8>) -> SearchResult<usize> {
    let mut found_index = vec.len();
    let mut found_count = 0;
    let mut found = false;
    let mut i = 0;

    while i < vec.len() - sub_vec.len() {
        if vec[i] == sub_vec[0] {
            for k in 0..sub_vec.len() {
                if vec[i+k] == sub_vec[k] {
                    found_index = i;
                    found_count += 1;
                    found = true;
                } else {
                    found = false;
                }
            }
            if found_count == sub_vec.len() {
                break;
            } else {
                found_count = 0;
            }
        }
        i += 1;
    }

    return if found && found_index < vec.len() {
        Ok(found_index)
    } else {
        Err(SearchError)
    }
}