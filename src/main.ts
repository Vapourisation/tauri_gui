import { createApp } from "vue";
import VueCookies from 'vue-cookies'
import * as VueRouter from "vue-router";

import "./styles.css";
import App from "./App.vue";
import Encrypt from "./views/Encrypt.vue";
import Decrypt from "./views/Decrypt.vue";
import Generate from "./views/Generate.vue";
import EncryptFile from "./views/EncryptFile.vue";
import DecryptFile from './views/DecryptFile.vue';
import StegoEncode from "./views/StegoEncode.vue";
import Home from "./views/Home.vue";
import StegoDecode from "./views/StegoDecode.vue";

const routes = [
    { path: '/', component: Home, name: 'Home' },
    { path: '/encrypt', component: Encrypt, name: 'Encrypt text' },
    { path: '/decrypt', component: Decrypt, name: 'Decrypt text' },
    { path: '/passphrase', component: Generate, name: 'Generate Password/passphrase' },
    { path: '/encrypt-file', component: EncryptFile, name: 'Encrypt file' },
    { path: '/decrypt-file', component: DecryptFile, name: 'Decrypt file' },
    { path: '/stego-encode', component: StegoEncode, name: 'Stego Encode' },
    { path: '/stego-decode', component: StegoDecode, name: 'Stego Decode' },
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: VueRouter.createWebHashHistory(),
    routes, // short for `routes: routes`
})

createApp(App)
    .use(router)
    .use(VueCookies)
    .mount("#app");
